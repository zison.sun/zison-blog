# zison's blog

- 编程技巧
  - [实现MEMCMP函数式宏](./program/macro_memcmp.md)
  - [自定义日志函数](./program/log_function.md)
- 模拟器编程技术研究
  1. [介绍](./emulator/introduction.md)
  2. [Introduction to the process of emulation](./emulator/)
  3. [CPU模拟中断]()
- 汇编以及机器码
  1. [机器码中的前缀](asm_bincode/bin_code_prefix.md)
- 构建
  - [gn简介](./build/gn/gn.md)
  - [ninja入门](./build/ninja/ninja入门.md)	
-  测试与漏洞挖掘
  - [AFL使用简介](./test/AFL/AFL使用简介/AFL使用简介.md)
  - [libFuzzer及AFL简介.md](./test/libFuzzer及AFL简介.md)
  - [libFuzzer.md](./test/libFuzzer.md)
  - [sanitize介绍与使用](./test/sanitizer/sanitize介绍与使用.md)

