# Ninja使用入门
## Ninja是什么
    
   Ninja 是google为解决Chrome项目编译太慢而开发的构建工具，对应现在广为使用的make工具。与make一样，Ninja也需要一个类似makefile的规则输入文件，Ninja的规则文件一般使用后缀.ninja。make具有很多功能：后缀规则，函数，内置规则，例如在构建源代码时搜索RCS文件,Ninja没有这些功能。Ninja的定位是一个注重速度的小型构建系统。

## Ninja build环境
   1. 安装依赖

   ```bash
    sudo apt-get install build-essential
    sudo apt-get install re2c python2.7
   ```

   2. 下载编译Ninja源码
    ```
    git clone git://github.com/ninja-build/ninja.git 
    cd ninja
    ./configure.py --bootstrap
    ```
    执行成功后将生成Ninja二进制文件,可以将其copy 到`usr/bin`等系统目录，或者使用export PATH 导出环境变量。
    
## 代码如何编译
其实对于C/C++和很多其他程序的编译都是一个道理，就是把一些源代码文件编译成目标文件，或者有的目标文件再编译到一个库里，然后再链接起来。所以Ninja的配置文件分为两个部分，rule和文件依赖关系。看个简单的例子:<br>

文件目录结构：
```
├── build.ninja
└── main.c
```
main.c:
```c++
#include "Hello.h"
int main(int argc, char *argv[])
{
    printf("sandeepin poi!");
    return 0;
}
```
build.ninja:
```
#设置.ninja_log、.ninja_deps 在当前工作目录的tmp目录下生成
builddir = ./tmp
# 指定ninja最小需要版本
ninja_required_version = 1.7

# 变量
CC = gcc
cflags = -Wall

# 编译规则
rule compile
  command = $CC -c $cflags $in -o $out
  description = 编译 $in 成为 $out
build main.o : compile main.c

# 链接规则
rule link
  command = $CC $DEFINES $INCLUDES $cflags $in -o $out
  description = 链接 $in 成为 $out
build main.exe : link main.o

# 编译all，就是做任务build main.exe
build all: phony main.exe

# 默认编译什么(单独运行ninja)
default all
```
运行结果:
```shell
#ninja
[2/2] 链接 main.o 成为 main.exe
#ls -la
drwxr-xr-x 3 zison-sun zison-sun  4096 4月  15 11:28 .
drwxr-xr-x 7 zison-sun zison-sun  4096 4月  15 11:06 ..
-rw-r--r-- 1 zison-sun zison-sun   597 4月  15 11:11 build.ninja
-rw-r--r-- 1 zison-sun zison-sun   101 4月  15 11:06 main.c
-rwxr-xr-x 1 zison-sun zison-sun 16416 4月  15 11:28 main.exe
-rw-r--r-- 1 zison-sun zison-sun  1504 4月  15 11:28 main.o
drwxr-xr-x 2 zison-sun zison-sun  4096 4月  15 11:23 tmp
```

## 在项目中使用Ninja的几种方式
   1. 基于cmake的工程，可以使用  cmake 的 -G 选项指定生成器为 ninja 来生成 build.ninja，cmake生成的可能会非常冗余。
   2. 使用GN 生成Ninja规则文件。具体生成方法请参阅 《GN入门》。
   3. android 项目中会使用Kati 想原有的makefile 转换为Ninja文件。非android项目应该也可以使用此工具，有待验证。
   4. 自己根据Ninja语法规则编写.ninja文件。 本文将基于此种方式，完成json解析库cjson编译项目的构建。    

### 运行Ninja

运行Ninja,默认Ninja在当前目录查找build.ninja文件，然后编译所有过期的target。你可以使用参数指定要编译的targer。
类似make 默认使用当前目录makefile ，可以通过make clean ,make xxx 等单独编译单个target。

## Ninja命令
   使用`ninja --h`查看ninja支持的参数
    
   ```bash
    --version  打印版本信息
    -v         显示构建中的所有命令行（这个对实际构建的命令核对非常有用）

    -C DIR     在执行操作之前，切换到`DIR`目录
    -f FILE    制定`FILE`为构建输入文件。默认文件为当前目录下的`build.ninja`。如 ./ninja -f demo.ninja

    -j N       并行执行 N 个作业。默认N=3（需要对应的CPU支持）。如 ./ninja -j 2 all
    -k N       持续构建直到N个作业失败为止。默认N=1
    -l N       如果平均负载大于N，不启动新的作业
    -n         排练（dry run）(不执行命令，视其成功执行。如 ./ninja -n -t clean 本来会清除文件，使用-n则不清除了)

    -d MODE    开启调试模式 (用 -d list 罗列所有的模式)
    -t TOOL    执行一个子工具(用 -t list 罗列所有子命令工具)。如 ./ninja -t query all
    -w FLAG    控制告警级别
   ```
  我们可以看到基本常用的选项与make一样。
  
### 1. 调试模式  
`ninja -d list`可以查看调试模式支持的模式
```
debugging modes:
  stats        print operation counts/timing info 					打印统计信息
  explain      explain what caused a command to execute 			解释导致命令执行的原因
  keepdepfile  don't delete depfiles after they're read by ninja 	读取depfile后，不删除它
  keeprsp      don't delete @response files on success 				读取@response后，不删除它
  nostatcache  don't batch stat() calls per directory and cache them 不对每个目录批量处理stat()调用和缓存它们
  multiple modes can be enabled via -d FOO -d BAR 					多模式调用可以接着几个-d
```
-d使用示例：<br>
<img src="ninjad.png" width = "500" height = "300" alt="d选项例子" align=center /><br>
-n是假执行，实际未产生文件，由于假执行，keepdepfile没起到效果，这个受限于编译器分析依赖，下面的统计信息就是stats效果，explain解释了为什么执行这些任务。
 
### 2.警告等级
ninja -w list相关，主要指定几种情况下告警级别是多少：
```
warning flags:
  dupbuild={err,warn}  multiple build lines for one target
  phonycycle={err,warn}  phony build statement references itself
  depfilemulti={err,warn}  depfile has multiple output paths on separate lines
```

### 3.Ninja工具集
`ninja -t list`可以查看 ninja 中集成了哪些工具.
 ```
 ninja subtools:
     browse  	在浏览器中浏览依赖关系图。（默认会在8080端口启动一个基于python的http服务）
     clean  	清除构建生成的文件
     commands  	罗列重新构建制定目标所需的所有命令
     deps  		显示存储在deps日志中的依赖关系
     graph  	为指定目标生成 graphviz dot 文件。如 ninja -t graph all |dot -Tpng -o graph.png
     query  	显示一个路径的inputs/outputs
     targets  	通过DAG中rule或depth罗列target
     compdb  	dump JSON兼容的数据库到标准输出
     recompact  重新紧凑化ninja内部数据结构
```
1. `-t deps` 显示依赖
  <br><img src="deps.png"  alt="deps.png" align=center/>
2. `-t commands`显示执行指令
  <br><img src="command.png"  alt="command.png" align=center/>
3. `-t targets`显示执行指令
  <br><img src="target.png"  alt="target.png" align=center/>
4. `-t graph`绘依赖图（没有安装graphviz，直接打印出dot文本）
 <br><img src="graph.png"  alt="graph.png" align=center/><br>
`sudo apt-get install graphviz graphviz-doc` 安装graphviz<br>
转图片（支持png、svg等，大图推荐svg渲染，相关dot参数见graphviz文档）：<br>
`ninja -t graph | dot -Tpng -o graph.png`
<br><img src="export_graph.png"  alt="export_graph.png" align=center/>

## 环境变量
通过环境变量NINJA_STATUS可以控制ninja打印进度状态的样式，有几个占位符：
```
%s 起始edges的数量。
%t 完成构建必须运行的edges总数。
%p 起始edges的百分比。
%r 当前运行的edges数。
%u 要开始的剩余edges数。
%f 完成的edges数。
%o 每秒完成edges的总速率
%c 当前每秒完成edges的速率（由-j或其默认值指定的构建的平均值）
%e 经过的时间（以秒为单位）。（自Ninja 1.2起可用。）
%% 一个普通的%字符。
```
默认进度状态为"[%f/%t] "（请注意尾随空格以与构建规则分开）。<br>
可能的进度状态的另一个示例可能是"[%u/%r/%f] "。<br>
尝试改为export NINJA_STATUS="[%p/%f/%t %e] "（Windows下set NINJA_STATUS="[%p/%f/%t %e] "）的效果如下：
<br><img src="env.png"  alt="env.png" align=center/>

## .ninja_log文件
`cat ./tmp/.ninja_log`
<br><img src="ninja_log.png"  alt="ninja_log" align=center/><br>
ninja_log每项含义依次为：开始时间、结束时间、mtime、output文件路径名、命令行hash。

其中mtime是输入文件们的最后修改时间的时间戳算出来的值，经测试，开始时间、结束时间、命令行hash均不会影响增量的判定。

## 其他
ninja检测的是任务名的文件是否生成
如果我让输出文件和任务名不一样，例如：
```
# 链接规则
rule link
  command = $CC $DEFINES $INCLUDES $cflags $in -o $xxx
  description = 链接 $in 成为 $xxx
build main.exe : link main.o
  xxx = cpoy_main.exe
```
编译结果：
<br><img src="alway_complie.png" alt="alway_complie.png" align=center/><br>
ninja每次都会重新编译。

## Ninja语法简介
### 概念
| 原语      | 译名    |  解释  |
| :--------: | :-----:  | :----  |
| edge     | 边 		|  即build语句，指定目标（输出）、规则与输入，是编译过程拓扑图中的一条边（edge）  |
| target   |   目标  |   编译过程需要产生的目标，由build语句指定   |
| output        |   输出   |  build语句的前半段，是target的另一种称呼  |
| input        |   输入   |  build语句的后半段，用来产生output的文件或目标，另一种称呼是依赖  |
| rule        |   规则   |  通过指定command与一些内置变量，决定如何从输入产生输出  |
| pool        |   池   |  一组rule或edge，通过指定其depth，可以控制并行上限  |
| scope        |   作用域   | 变量的作用范围，有rule与build语句的块级，也有文件级别。rule也有scope  |
有些概念我们前面已经接触过了。

### 关键字
| 原语        |  作用  		|
| :-------:| :----  	   |
| build    |  定义一个edge  |
| rule     |  定义一条rule  |
| pool     |  定义一个pool  |
| default  |  指定默认的一个或多个target  |
| include  |  添加一个ninja文件到当前scope|
| subninja |  添加一个ninja文件，其scope与当前文件不同 |
| phony    | 一个内置的特殊规则，指定非文件的target ，phony可用于为其他目标创建别名。<br>例如：构建foo：伪造some/file /in/a/faraway/subdir/foo |

### 变量
变量有两种，自定义变量与内置变量，二者都可以通过var = str的方式定义，通过$var或${var}的方式引用。 变量类型只有一种，那就是字符串。
 
| 内置变量       |  作用    |
| :-------:| :----  	   |
| builddir    |  指定一些文件的输出目录，如.ninja_log、.ninja_deps等。  |
| ninja_required_version  |  指定ninja命令的最低版本，低于此值，Ninja执行会报错退出  |

例如：
```
builddir = ./tmp  				（内置变量：设置.ninja_log、.ninja_deps 在当前工作目录的tmp目录下生成）
ninja_required_version = 1.10

cc=gcc 						   （自定义变量：设置cc =gcc）
cflags= -g -c
```

### rule
以下是rule代码块的示例。<br>通常，一个rule就是通过 \${in} 输入的目标列表，生成\${out}的输出目标列表。可以理解成一个函数，输入 \$in ,输出\$out。
```
rule cc 
  command = $cc $cflags $in -o $out
  
build 
```

目标一般都是文件。 例外是，有一个内置的特殊规则phony，可以指定非文件目标.
除了可以自定义变量以外，包括command在内，rule还有以下内置变量：

| 内置变量       |  作用    |
| :-------:		| :----  	   |
|command		|定义一个规则所必备的变量，指定实际执行的命令。|
|description	|command的说明，会替代command在无-v时打印。|
|generator		|指定后，这条rule生成的文件不会被默认清理。|
|in				|空格分割的input列表。|
|in_newline		|换行符分割的input列表。|
|out			|空格分割的output列表。|
|depfile		|指定一个Makefile文件作为额外的显式依赖。|
|deps			|指定gcc或msvc方式的依赖处理。|
|msvc_deps_prefix	|在deps=msvc情况下，指定需要去除的msvc输出前缀。|
|restat				|在command执行结束后，如果output时间戳不变，则当作未执行。|
|rspfile, rspfile_content|同时指定，在执行command前，把rspfile_content写入rspfile文件，执行成功后删除。|

### build
build语句声明输入文件和输出文件之间的关系。它们以build关键字开头，格式为 `build outputs: rulename inputs` 。
这样的声明表明所有输出文件都来自输入文件。当输出文件丢失或输入更改时，Ninja将运行规则以重新生成输出。
在一个build块的范围内（包括对其相关联的rule），变量\$in是输入列表，变量\$out是输出列表。

build语句后可以紧跟一组缩进的key = value 对。
build 可以有局部变量，类似C的函数内使用局部变量，而不会使用外部全局变量一样。例如：
```
cflags = -Wall -Werror 
rule cc 
  command = gcc $ cflags -c $ in -o $ out 
＃如果未指定，构建将获取外部$ cflags。
build foo.o：cc foo.c 
#但是，您可以为特定的构建屏蔽诸如cflags之类的变量。
构建special.o：cc special.c 
  cflags = -Wall 

＃仅在special.o的范围内隐藏了该变量；
＃随后的构建行获取外部（原始）标志。
build bar.o：cc bar.c
```
### phony rule
特殊规则名称phony可用于为其他目标创建别名。例如：
`build foo: phony some/file/in/a/faraway/subdir/foo`<br>
这使ninja foo构建路径更长。在语义上，该 phony规则等效于command什么也不做的普通规则，但是pony规则是特别的，因为它们在运行时不会打印，也不会对作为命令一部分打印的命令计数有所贡献构建过程。

## 使用ninja构建cJSON 
下载 cjson 代码 `git clone https://github.com/DaveGamble/cJSON.git`<br>
删除除源码 的文件，同下
 ```
 zison-sun@zison-sun-PC:~/work/cJSON$ tree -a
.
├── build.ninja
├── cJSON.c
├── cJSON.h
├── cJSON_Utils.c
├── cJSON_Utils.h
└── test.c
 ```
 
 build.ninja:
```bash
builddir = ./tmp
CC = gcc
CFLAGS = -g -Wall -Werror -fPIC
DEFINES = -DDEBUG
INCLUDES = -i ./
SOFLAGS = -shared  -fPIC 
LDFLAGS = -Wl,-rpath=`pwd`

rule cc
  command = $CC $cflags $INCLUDE $DEFINES -c $in -o $out
rule link
  command = $CC $LDFLAGS $in -o $out
rule solink
  command = $CC $SOFLAGS $FLAGS  -o $out $in
  
build test :link libcjson.so test.o
build test.o : cc test.c
build libcjson.so : solink cJSON.c cJSON_Utils.c
```
执行`ninja`编译，`./test` 执行如下：
<br><img src="result.png" alt="result.png" align=center/><br>

## 总结：
  不管是make 还是Ninja 都是 底层基于 gcc clang等编译工具。只是语法有所不同，设计理念也有所不同。明白这点，入门就没那么难了。后续我们将更加深入的学习 gn , ninja 这套构建工具。
  


