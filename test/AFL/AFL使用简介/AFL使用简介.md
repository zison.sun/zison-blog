# AFL使用指南

## AFL简介
AFL(American Fuzzy Lop)是目前最高级的Fuzzing测试工具之一 .当测试的程序有源码时,AFL通过对源码重新编译时插桩(插入分析代码)的方法来探测程序内部的执行路径.相对于其他fuzzer,AFL-Fuzz具有更低的性能消耗,更有效的fuzzing策略和tricks最小化技巧,只需简单的配置即可处理复杂的程序.当然,对于没有源码的可执行程序,AFL也可进行处理,但需要QEUM模拟器的支持.

源码模式下基本流程:
1. 从源码编译程序时进行插桩，以记录代码覆盖率（Code Coverage）；
2. 选择一些输入文件，作为初始测试集加入输入队列（queue）；
3. 将队列中的文件按一定的策略进行“突变”；
4. 如果经过变异文件更新了覆盖范围，则将其保留添加到队列中;
5. 上述过程会一直循环进行，期间触发了crash的文件会被记录下来。
![AFL流程](./flow.png)

## AFL环境构建
1. 源码安装AFL
```bash
git clone https://github.com/google/AFL.git
cd AFL
make
```
AFL目录下将生成如下可执行文件
```
lrwxrwxrwx  1 zison zison      7 7月  16 10:17 afl-clang -> afl-gcc
lrwxrwxrwx  1 zison zison      7 7月  16 10:17 afl-clang++ -> afl-gcc
lrwxrwxrwx  1 zison zison      7 7月  16 10:17 afl-g++ -> afl-gcc
-rwxr-xr-x  1 zison zison  24504 7月  16 10:17 afl-gcc
-rwxr-xr-x  1 zison zison  45096 7月  16 10:17 afl-as
-rwxr-xr-x  1 zison zison 339344 7月  16 10:17 afl-fuzz
-rwxr-xr-x  1 zison zison  11391 7月  16 10:12 afl-cmin
-rwxr-xr-x  1 zison zison  71584 7月  16 10:17 afl-tmin
-rwxr-xr-x  1 zison zison  61384 7月  16 10:17 afl-analyze
-rwxr-xr-x  1 zison zison  28904 7月  16 10:17 afl-gotcpu
-rwxr-xr-x  1 zison zison   4912 7月  16 10:12 afl-plot
-rwxr-xr-x  1 zison zison  55784 7月  16 10:17 afl-showmap
-rwxr-xr-x  1 zison zison   3711 7月  16 10:12 afl-whatsup
```
**afl-clang-fast**: 顾名思义，会比afl-clang,afl-gcc 更快一些。但是需要clang的支持。确保本机安装了clang,进入AFL
的llvm_mode目录下 执行 
```bash
LLVM_CONFIG=`which llvm-config-7`
make
```
在AFL目录下会多出：<br>
-rwxr-xr-x  1 zison zison   33152 7月  16 10:28 afl-clang-fast
lrwxrwxrwx  1 zison zison      14 7月  16 10:28 afl-clang-fast++ -> afl-clang-fast

2 二进制安装:
```bash
sudo apt install afl
将会同时安装下列软件：
afl-clang afl-doc clang-6.0 libclang-common-6.0-dev libclang1-6.0 libllvm6.0 libomp-dev llvm-6.0 llvm-6.0-dev llvm-6.0-runtime
```
我们可以看到没有afl-clang-fast 。当然不影响使用，程序的使用方法是一样的。

## 语料库
### 什么是语料库
作为模糊测试，AFL需要提供初始的种子输入，我们称之为语料库(Corpus) , 语料库为fuzzer 提供了输入基础，fuzzer给予语料库进行变异，这对结构化输入的目标程序非常重要，典型的就是有固定文件头的图片处理程序等等。
一般来说在进行fuzz之前构建一份有效的语料库是十分有必要的，这将作为程序开始时的种子。

语料库的信息来源主要如下:

- 使用项目自身提供的测试用例
- 目标程序bug提交页面
- 使用格式转换器，用从现有的文件格式生成一些不容易找到的文件格式：
- afl源码的testcases目录下提供了一些测试用例
- 其他开源的语料库

### 语料库蒸馏
网上找到的一些大型语料库中往往包含大量的文件，其中有的文件会触发相同的代码路径，同样的覆盖率，这时就需要对其精简，这个工作有个术语叫做——语料库蒸馏（Corpus Distillation）。AFL提供了两个工具来帮助我们完成这部工作——afl-cmin和afl-tmin。我们将在后面的复杂示例中进行蒸馏。

## AFL常用工具
#### 1. afl-gcc，afl-g++，afl-clang，afl-clang++
经过afl 包装过的编译器，与使用gcc,g++等方法参数一样。

#### 2. afl-cmin
afl-cmin工具可以减小corpus的大小，这个工具可以得到一个更小的子集（原case集合的子集），这个子集可以提供同样的edge coverage效果

通常用法：
```bash
//afl-cmin -i 测试用例文件夹 -o 筛选后的测试用例文件夹 [可能会用到其他操作] 测试程序文件
//这里的用法如下
afl-cmin -i fuzz_in -o fuzz_in_cmin ./target
```
#### 2. afl-tmin
afl-tmin工具精简单个case文件，已达到同样的效果。
afl-tmin有两种工作模式，instrumented mode和crash mode。默认的工作方式是instrumented mode,如果指定了参数-x，即crash mode，会把导致程序非正常退出的文件直接剔除。afl-tmin只针对单个文件，所以当有多文件需要处理时，一般我们需要写个脚本或者一段命令多次执行afl-tmin。

```bash
#遍历执行
for file in  fuzz_in_cmin/* ; do afl-tmin -i $file -o $file.tmin ./target ; done
#tmin 目录
mkdir fuzz_in_tmin
#将裁剪过的case放入tmin 目录
cp fuzz_in_cmin/*.tmin  fuzz_in_tmin
```
#### 2. afl-fuzz
使用afl-fuzz 开始模糊测试

afl-fuzz 参数说明：
- -i：指定测试样本所在目录；
- -o：指定测试结果存放目录；
- -M：运行主(Master) Fuzzer；
- -S：运行从属(Slave) Fuzzer；
- -t：设置程序运行超时值，单位为 ms；
- -m：最大运行内存，单位为 MB；

常见使用方式：`afl-fuzz -i corpus -o outputs  ./target`
另外 afl-fuzz 支持使用@@ 作为参数替换输入文件<br>
`afl-fuzz -i corpus -o outputs  ./target @@`<br>
事实上：@@会被替换成outputs/.cur_input  而.cur_input中的内容 每次都会变化。


## AFL的简单使用示范
#### 1. 编写target.c
  首先创建一个简单的target.c源文件，其功能无外乎接受一行命令行输入（一个整数、一个字符、再一个整数），然后根据中间这个字符当作运算符，输出四则运算结果。
```c
#include <stdio.h>

int main(int argc, char *argv[])
{
    int a, b;
    char op;
    scanf("%d%c%d", &a, &op, &b);
    int result;
    switch (op) {
        case '+':
            result = a + b;
            break;
        case '-':
            result = a - b;
            break;
        case '*':
            result = a * b;
            break;
        case '/':
            result = a / b;
            break;
        default:
            return 1;
    }
    printf("%d\n", result);
    return 0;
}
 ```
可以看见，这个被测程序的内容和普通的用户交互程序一模一样，看不出来任何不同，就是以stdin/stdout作为交互的输入输出，以main函数作为执行入口。AFL会给stdin输入不同的数据。

#### 2.插桩编译
除了使用的命令需要带上afl-前缀，编译过程和普通gcc编译一样:`afl-clang-fast -o target target.c`

#### 3. 准备种子语料库

```base
mkdir good-seeds bad-seeds
echo '1+1' > good-seeds/any-seed
echo 'this is a bad seed' > bad-seeds/any-seed
```
#### 4.开始测试
执行`afl-fuzz -i good-seeds/ -o good-outputs -- ./target`<br>
程序会输出如下：
```
                       american fuzzy lop 2.57b (target)

┌─ process timing ─────────────────────────────────────┬─ overall results ─────┐
│        run time : 0 days, 0 hrs, 0 min, 4 sec        │  cycles done : 25     │
│   last new path : 0 days, 0 hrs, 0 min, 4 sec        │  total paths : 5      │
│ last uniq crash : 0 days, 0 hrs, 0 min, 4 sec        │ uniq crashes : 1      │
│  last uniq hang : none seen yet                      │   uniq hangs : 0      │
├─ cycle progress ────────────────────┬─ map coverage ─┴───────────────────────┤
│  now processing : 1 (20.00%)        │    map density : 0.00% / 0.02%         │
│ paths timed out : 0 (0.00%)         │ count coverage : 1.00 bits/tuple       │
├─ stage progress ────────────────────┼─ findings in depth ────────────────────┤
│  now trying : havoc                 │ favored paths : 5 (100.00%)            │
│ stage execs : 225/256 (87.89%)      │  new edges on : 5 (100.00%)            │
│ total execs : 38.8k                 │ total crashes : 48 (1 unique)          │
│  exec speed : 6981/sec              │  total tmouts : 0 (0 unique)           │
├─ fuzzing strategy yields ───────────┴───────────────┬─ path geometry ────────┤
│   bit flips : 4/160, 1/155, 0/145                   │    levels : 2          │
│  byte flips : 0/20, 0/15, 0/5                       │   pending : 0          │
│ arithmetics : 0/1115, 0/125, 0/0                    │  pend fav : 0          │
│  known ints : 0/104, 0/420, 0/220                   │ own finds : 4          │
│  dictionary : 0/0, 0/0, 0/0                         │  imported : n/a        │
│       havoc : 0/36.1k, 0/0                          │ stability : 100.00%    │
│        trim : n/a, 0.00%                            ├────────────────────────┘
└─────────────────────────────────────────────────────┘          [cpu000: 63%]
```
使用 `cat good-outputs/.cur_input` 能看到当前输入。

上图中显示已经找到了一个crash了，ctrl-C结束fuzz，可以看到当前目录下已经多出了good-outputs目录，这是本次模糊测试的结果。

引起崩溃的测试样例会位于good-outputs/crashes文件夹下，文件名大致形如`id\:000000\,sig\:08\,src\:000002\,op\:flip1\,pos\:2`，使用xxd 查看其内容为：
`00000000: 312f 300a                                1/0.`<br>
其中包含1/0三个可打印字符和一个换行符。并未出乎意料，就是这种输入会导致程序中发生除零意外。
另外，还可以对另一个坏种子语料库重复实验`afl-fuzz -i bad-seeds/ -o bad-outputs -- ./hello`
结果照样能找到类似的引发除零错误的样例，只不过这次消耗的时间会比优质种子消耗的时间更多。由此可见语料库会对fuzzy的效率有影响。

## 使用AFL测试大型程序
我们基于libpng(version 1.6.26) 演示更多的afl使用方法。

#### 1.编译libpng库：
```bash
tar xzf libpng.tgz
cd libpng
```
禁止CRC：
```
--- pngrutil.c.orig	2014-06-12 03:35:16.000000000 +0200
+++ pngrutil.c	2014-07-01 05:08:31.000000000 +0200
@@ -268,7 +268,11 @@
    if (need_crc != 0)
    {
       crc = png_get_uint_32(crc_bytes);
-      return ((int)(crc != png_ptr->crc));
+      return ((int)(1 != 1));
    }
```
禁用libpng log：
```
cat scripts/pnglibconf.dfa | sed -e "s/option STDIO/option STDIO disabled/" \
> scripts/pnglibconf.dfa.temp
mv scripts/pnglibconf.dfa.temp scripts/pnglibconf.dfa
# build the library.
autoreconf -f -i
CC="afl-clang-fast"  CFLAGS="-DPNG_SIMPLIFIED_WRITE_STDIO_SUPPORTED -DPNG_STDIO_SUPPORTED"  ./configure  --disable-shared
make
```
#### 2. Fuzzer程序

使用`afl-clang-fast -o target libpng/contrib/libtests/readpng.c libpng/.libs/libpng16.a -lz -lm`   

看下现在的文件列表：
```shell
zison@zison-PC:~/work/fuzzy/pngfuzzer$ ls -la
总用量 1056
drwxr-xr-x  4 zison zison    4096 7月  16 15:26 .
drwxr-xr-x  9 zison zison    4096 7月  16 14:02 ..
drwxr-x--- 11 zison zison    4096 7月  16 15:25 libpng
drwxr-xr-x  2 zison zison   32768 7月  16 13:59 corpus
-rwxr-xr-x  1 zison zison 1022912 7月  16 15:26 target
```
其中corpus 中文件来自于https://github.com/Dor1s/libfuzzer-workshop/lessons/09 中的 测试用例。

#### 3. 语料库蒸馏
执行`afl-cmin -i corpus -o cmin_out ./target`，显示如下：
```
corpus minimization tool for afl-fuzz by <lcamtuf@google.com>

[*] Testing the target binary...
[+] OK, 482 tuples recorded.
[*] Obtaining traces for input files in 'corpus'...
    Processing file 76/76... 
[*] Sorting trace sets (this may take a while)...
[+] Found 1072 unique tuples across 76 files.
[*] Finding best candidates for each tuple...
    Processing file 76/76... 
[*] Sorting candidate list (be patient)...
[*] Processing candidates and writing output files...
    Processing tuple 1072/1072... 
[+] Narrowed down to 25 files, saved in 'cmin_out'.
```
cmin之前:
![cmin之前](./cmin_bef.png)
cmin之后:<br>
![cmin之后](./cmin-aft.png)<br>
可以看到76个文件被裁剪为了25个。

```shell
mkdir tmin_out
for file in cmin_out/* ; do afl-tmin -i $file -o $file.tmin ./target; done 
mv cmin_out/*.tmin  tmin_out
```
afl-tmin开始最每个testcase 进行裁剪，这可能会花费一些时间。
```
afl-tmin 2.57b by <lcamtuf@google.com>

[+] Read 672 bytes from 'cmin_out/axis_aligned.png'.
[*] Performing dry run (mem limit = 50 MB, timeout = 1000 ms)...
[+] Program terminates normally, minimizing in instrumented mode.
[*] Stage #0: One-time block normalization...
[+] Block normalization complete, 0 bytes replaced.
[*] --- Pass #1 ---
[*] Stage #1: Removing blocks of data...
    Block length = 64, remaining size = 672
 ...
    Block length = 1, remaining size = 672
[+] Block removal complete, 0 bytes deleted.
[*] Stage #2: Minimizing symbols (126 code points)...
[+] Symbol minimization finished, 4 symbols (4 bytes) replaced.
[*] Stage #3: Character minimization...
[+] Character minimization done, 9 bytes replaced.
[*] --- Pass #2 ---
[*] Stage #1: Removing blocks of data...
    Block length = 64, remaining size = 672
  ...
    Block length = 1, remaining size = 672
[+] Block removal complete, 0 bytes deleted.

     File size reduced by : 1.00% (to 672 bytes)
    Characters simplified : 1.93%
     Number of execs done : 3508
          Fruitless execs : path=2900 crash=0 hang=594

[!] WARNING: Frequent timeouts - results may be skewed.
[*] Writing output to 'cmin_out/axis_aligned.png.tmin'...
[+] We're done here. Have a nice day!
```
截取其中一个afl-tmin的显示，可以看到 没有精简-_-! 

#### 4.开始测试
```
afl-fuzz -i tmin_out -o out ./target
```
屏幕显示：<br>
![afl-fuzz状态图](./afl-fuzz.png)<br>
我们可以看到其中有11个 hangs,也就有11个testcase 会让程序卡住。

## AFL状态窗口
AFL中需要关注的状态值：<br>
### process timing
- process timing:Fuzzer运行时长、以及距离最近发现的路径、崩溃和挂起经过了多长时间
- runtime: 已运行时间
- last new path: 距离最后发现新路径的时间，
- last uniq crash: 距离最后一次crash时间
- last uniq hang: 距离最后一次挂起时间

### overall results
- Overall results：Fuzzer当前状态的概述
- cycles done: 真个输入队列完成一个变化周期的次数
- total paths: 总代码路径
- uniq crashes: 记录的唯一崩溃次数
- uniq hangs:  遇到的唯一挂起次数
### cycle progress
- cycle progress：一个循环的进度。
- now proeessing: cycles 的进度 ，当为100% 后 cycles done 加 1。
### 其他
 - Map coverage：目标二进制文件中的插桩代码所观察到覆盖范围的细节
 - Stage progress：Fuzzer现在正在执行的文件变异策略、执行次数和执行速度
 - Findings in depth：有关我们找到的执行路径，异常和挂起数量的信息。
 - Fuzzing strategy yields：关于突变策略产生的最新行为和结果的详细信息。
 - Path geometry：有关Fuzzer找到的执行路径的信息。
 - CPU load：CPU利用率
其中发现last new path 太久，则表示程序已经不能再发现新路径，基本可以ctrl+c结束掉了，或者cycles done 变成绿色 也可能继续跑也跑不出更多的crash hangs了。

### 并行测试
#### 单系统并行测试
如果你有一台多核心的机器，可以将一个afl-fuzz实例绑定到一个对应的核心上，也就是说，机器上有几个核心就可以运行多少afl-fuzz 实例，这样可以极大的提升执行速度。使用下面命令查看cpu核心数:
```
cat /proc/cpuinfo| grep "cpu cores"| uniq
cpu cores	: 4
```
afl-fuzz并行Fuzzing，一般的做法是通过-M参数指定一个主Fuzzer(Master Fuzzer)、通过-S参数指定多个从Fuzzer(Slave Fuzzer)。

```
afl-fuzz -i cmin_out -o out -M fuzzer0 ./target 
afl-fuzz -i cmin_out -o out -S fuzzer1 ./target 
afl-fuzz -i cmin_out -o out -S fuzzer2 ./target 
afl-fuzz -i cmin_out -o out -S fuzzer3 ./target 
```
这两种类型的Fuzzer执行不同的Fuzzing策略，前者进行确定性测试（deterministic ），即对输入文件进行一些特殊而非随机的的变异；后者进行完全随机的变异。

可以看到这里的-o指定的是一个同步目录，并行测试中，所有的Fuzzer将相互协作，在找到新的代码路径时，相互传递新的测试用例，如下图中以Fuzzer0的角度来看，它查看其它fuzzer的语料库，并通过比较id来同步感兴趣的测试用例。
![单机同步](./multi.png)

afl-gotcpu工具可以查看每个核心使用状态
```bash
afl-gotcpu
afl-gotcpu 2.57b by <lcamtuf@google.com>
[*] Measuring per-core preemption rate (this will take 1.00 sec)...
    Core #1: OVERBOOKED (526%)
    Core #2: OVERBOOKED (439%)
    Core #0: OVERBOOKED (421%)
    Core #3: OVERBOOKED (428%)

>>> FAIL: All cores are overbooked. <<<
```
#### 多系统并行测试
多系统并行的基本工作原理类似于单系统并行中描述的机制，你需要一个简单的脚本来完成两件事。在本地系统上，压缩每个fuzzer实例目录中queue下的文件，通过SSH分发到其他机器上解压。

来看一个例子，假设现在有其他两台机器，基本环境如下：
分别使用-M -S 指定了 **SESSIONxxx** 的路径

|	fuzzer1	|	fuzzerr2	|
| 	----	|	-----		|
| 10.20.12.36|10.20.12.236|
| 当前运行2个实例 |当前运行4个实例|

对于有密码的机器，为了能够自动同步数据，需要安装sshpass
`sudo apt install sshpass`

执行下面脚步：
```shell
#!/bin/sh
​
# 所有要同步的主机
FUZZ_HOSTS='10.20.12.36 10.20.12.236'
# SSH user
FUZZ_USER=uos
FUZZ_PWD=1
# 同步目录
SYNC_DIR='/uos/syncdir'
# 同步间隔时间
SYNC_INTERVAL=$((30 * 60))

if [ "$AFL_ALLOW_TMP" = "" ]; then
  if [ "$PWD" = "/tmp" -o "$PWD" = "/var/tmp" ]; then
    echo "[-] Error: do not use shared /tmp or /var/tmp directories with this script." 1>&2
    exit 1
  fi
fi
​
rm -rf .sync_tmp 2>/dev/null
mkdir .sync_tmp || exit 1

while :; do
  # 打包所有机器上的数据
  for host in $FUZZ_HOSTS; do
    echo "[*] Retrieving data from ${host}..."
    sshpass -p $FUZZ_PWD ${FUZZ_USER}@${host} \
      "cd '$SYNC_DIR' && tar -czf - SESSION* " > ".sync_tmp/${host}.tgz"
  done

  # 分发数据
  for dst_host in $FUZZ_HOSTS; do
    echo "[*] Distributing data to ${dst_host}..."
    for src_host in $FUZZ_HOSTS; do
      test "$src_host" = "$dst_host" && continue
      echo "    Sending fuzzer data from ${src_host}..."
      sshpass -p $FUZZ_PWD ${FUZZ_USER}@$dst_host \
        "cd '$SYNC_DIR' && tar -xkzf - &>/dev/null" < ".sync_tmp/${src_host}.tgz"
    done
  done

  echo "[+] Done. Sleeping for $SYNC_INTERVAL seconds (Ctrl-C to quit)."
  sleep $SYNC_INTERVAL
done
```

## 其他fuzzy工具
除了AFL ,常用的还有libFuzzer,Peach Fuzz 等。其中 libFuzzer 使用较多。我们就讲解下libFuzzer 与AFL 区别。

1. AFL可与gcc或clang一起使用，但libFuzzer由LLVM开发，仅clang支持。
2. libfuzzer 需要手动编写一个int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size)`函数作为接口作为Fuzz的入口。 而 afl   只需要 stdin 与 stdout 即可
3. 由于libFuzzer是在进程内多次调用一个函数入口，比AFL省去了进程创建，销毁的开销，所以效率会比AFL 快些。同时libFuzeer适用与模块初始化的测试，类似下面：
 ```c
 bool inited = false;
 int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size)
 {
 	if (!inited)
    {
    	init(); 
        inited = true;
    }
    test(data,size);
 }
 ```
 坏处就是因为在同一进程内，会存在全局污染的问题，例如使用统一全局变量等等。

## 对于GUI程序的模糊测试
 由于GUI程序长时间驻留系统，不会退出。一般测试方法是编写单独的测试模块对GUI程序中单个功能进行测试，由此能提升效率。
 不能拆分的情况下，可以给GUI 程序合适的地方加入exit 进行退出。

## 总结：
 模糊测试是以来大量输入进行类似暴力破解的测试方法，所以 需要在构造fuzzer程序时，尽量精简，拆分为小模块单独测试，同时良好的语料库能大大减少AFL无意义变异的时间。另外测试周期可能会很长。
