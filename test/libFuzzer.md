
# libFuzzer介绍及使用

> 本文以 libfuzzer-workshop 为基础 介绍 libFuzzer 的使用。

## 基础概念
### 1. libFuzzer简介
libFuzzer 是一个in-process，coverage-guided，evolutionary 的 模糊测试引擎，是 LLVM 项目的一部分。libFuzzer和被测试的模块链接在一起，通过一个模糊测试入口函数，将测试用例传递给要被测试的模块。libFuzzer会跟踪哪些代码执行路径，然后在语料库上进行变异，来使代码覆盖率最大化。代码覆盖率的信息由LLVM 的SanitizerCoverage插桩提供。

### 2. 模糊测试的种类
- Generation Based ：通过对目标协议或文件格式建模的方法，从零开始产生测试用例，没有先前的状态
- Mutation Based ：基于一些规则，从已有的数据样本或存在的状态变异而来
- Evolutionary ：包含了上述两种，同时会根据代码覆盖率的回馈进行变异。

libFuzzer是evolutionary类型的，也就是说它只是从零开始产生测试用例，也可以预先提供语料库，在语料库的基础上进行变异。

### 3. 语料库
模糊测试时，提供给AFL的初始输入，我们称之为语料库(Corpus)。语料库包含了各种被测代码的有效和无效输入集合。例如，对于图形库，初始语料库可能包含各种不同的小PNG/JPG/GIF文件。libFuzzer基于语料库进行变异，以达到发现新的代码路径，更高的代码覆盖率的目的。

LibFuzzer可以在没有任何初始种子的情况下工作，但是如果被测库接受复杂的结构化输入，效率将会降低。

语料库的信息来源主要如下:

- 使用项目自身提供的测试用例
- 目标程序bug提交页面
- 使用格式转换器，用从现有的文件格式生成一些不容易找到的文件格式：
- afl源码的testcases目录下提供了一些测试用例
- 其他开源的语料库

### 4.语料库蒸馏
网上找到的一些大型语料库中往往包含大量的文件，其中有的文件会触发相同的代码路径，同样的覆盖率，这时就需要对其精简，这个工作有个术语叫做——语料库蒸馏（Corpus Distillation）。我们可以使用 -merge=1 参数达到目的：
```
mkdir NEW_CORPUS_DIR  # Store minimized corpus here.
./my_fuzzer -merge=1 NEW_CORPUS_DIR FULL_CORPUS_DIR
```

### 5.语料库的使用
测试模块与libFuzzer一起生成的程序，我们称之为**模糊器**，在模糊器后面增加语料库路径即可。
`./my_fuzzer CORPUS_DIR  # -max_len=1000 -jobs=20 ...`

### 参数选项

要运行模糊器，请传递零个或多个语料库目录作为命令行参数。模糊器将从每个语料库目录中读取测试输入，并且所生成的任何新测试输入将被写回到第一个语料库目录中：
```
./fuzzer [-flag1=val1 [-flag2=val2 ...] ] [dir1 [dir2 ...] ]
```
如果将文件列表（而不是目录）传递到模糊器，则它将重新运行这些文件作为测试输入，但不会执行任何模糊测试,及不会根据此输入进行变异，产生更多的testcase。

- help:打印帮助信息（-help=1）。
- seed:随机种子。如果为0（默认值），则生成种子。
- runs:单个测试运行的次数，-1（默认）为无限期运行。
- max_len:测试输入的最大长度。如果为0（默认值），则libFuzzer尝试根据语料库猜测一个好的值（并报告该值）。
- len_control:首先尝试生成小的输入，然后随着时间的推移尝试更大的输入。指定增加长度限制的速率（更小==更快）。默认值为100。如果为0，请立即尝试使用最大为max_len的输入。
- timeout:超时（以秒为单位），默认为1200。如果输入的时间长于此超时时间，则该过程将视为失败情况。
- rss_limit_mb:内存使用限制，以Mb为单位，默认为2048。使用0禁用限制。如果输入需要执行的RSS存储器数量超过此数量，则该过程将视为失败情况。每秒在单独的线程中检查一次限制。如果不使用ASAN / MSAN，则可以使用'ulimit -v'。
- malloc_limit_mb:如果非零，则如果目标尝试通过一个malloc调用分配此Mb数量，则模糊器将退出。如果为零（默认），则应用与rss_limit_mb相同的限制。
- timeout_exitcode:如果libFuzzer报告超时，则使用退出代码（默认为77）。
- error_exitcode:如果libFuzzer本身（不是消毒剂）报告错误（泄漏，OOM等），则使用退出代码（默认值为77）。
- max_total_time:如果为正，则表示运行模糊器的最长时间（以秒为单位）。如果为0（默认值），则无限期运行。
- merge:如果设置为1，则来自第二，第三等语料库目录的任何触发新代码覆盖的语料库输入将合并到第一个语料库目录中。默认值为0。此标志可用于最小化语料库。
- merge_control_file:指定用于合并过程的控制文件。如果合并进程被杀死，它将尝试使该文件处于适合于继续合并的状态。默认情况下，将使用一个临时文件。
- minimize_crash:如果为1，则最小化提供的崩溃输入。与-runs = N或-max_total_time = N一起使用以限制尝试次数。
- reload:如果设置为1（默认值），则将定期重新读取语料库目录以检查是否有新输入；否则，将重新输入。这允许检测其他模糊处理过程发现的新输入。
- jobs:要完成的模糊测试作业数。默认值为0，它将运行一个模糊测试过程，直到完成为止。如果该值> = 1，则在并行的独立工作进程的集合中运行执行模糊测试的作业数量。每个这样的工作进程都有 stdout/ stderr重定向到fuzz-<JOB>.log。
- workers:运行模糊测试作业以完成操作的同时工作进程数。如果使用0（默认值），则使用。min(jobs, NumberOfCpuCores()/2)
- dict:提供输入关键字的字典；参见字典。
- use_counters:使用覆盖率计数器来生成命中代码块的频率的近似计数；默认为1。
- reduce_inputs:尝试减少输入的大小，同时保留其全部功能集；默认为1。
- use_value_profile:使用值简档到导向语料库扩张; 默认为0。
- only_ascii:如果为1，则仅生成ASCII（isprint``+``isspace）输入。预设为0。
- artifact_prefix:提供将模糊工件（崩溃，超时或缓慢输入）另存为时要使用的前缀$(artifact_prefix)file。默认为空。
- exact_artifact_path:如果为空则忽略（默认）。如果为非空，则将失败（崩溃，超时）时的单个工件写为$(exact_artifact_path)。这-artifact_prefix将覆盖 文件名，并且不会在文件名中使用校验和。请勿将相同的路径用于多个并行进程。
- print_pcs:如果为1，则打印出新覆盖的PC。预设为0。
- print_final_stats:如果为1，则在出口处打印统计信息。预设为0。
- detect_leaks:如果为1（默认值）并且启用了LeakSanitizer，则尝试在模糊测试期间检测内存泄漏（即，不仅在关闭时）。
- close_fd_mask:指示输出流在启动时关闭。注意，这将从目标代码中删除诊断输出（例如，断言失败时的消息）。
	- 0（默认）：既不关闭stdout也不关闭stderr
	- 1：关闭 stdout
	- 2：关闭 stderr
	- 3：同时关闭stdout和stderr。
- 要获取完整的标志列表，请使用来运行模糊器二进制文件-help=1。

## 简单示例
这一节，我们使用一个小例子来演示libFuzzer的基本使用方法。
```
#include <stdint.h>
#include <stddef.h>                                                                                  

bool DoSomethingInterestingWithMyAPI(const uint8_t* data, size_t size) {
  bool result = false;
  if (size >= 3) {
    result = data[0] == 'F' 
             data[1] == 'U' 
             data[2] == 'Z' 
             data[3] == 'Z';
  }

  return result;
}

// first_fuzzer.cc
extern "C" int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {
  DoSomethingInterestingWithMyAPI(Data, Size);
  return 0;  // Non-zero return values are reserved for future use.
}
```
LLVMFuzzerTestOneInput 是libFuzzer的提供给测试程序的入口函数，libFuzzer对多次调用此函数，每次传入不同参数。我们可以改造这个程序，将Data的内容以及Size写入日志文件，会发现每次进入此函数，Data的内容是一直变化的，Size也会变化。在这段示例中，如果 size >=3 ,且Data前面3个字节分别等于'F','U','Z',程序就会访问 data[3]，造成地址越界的错误。我们使用clang++ 编译后执行，查看具体效果。
`clang++ -g -std=c++11 -fsanitize=address,fuzzer -c first_fuzzer.cc -o first_fuzzer`

-fsanitize=address： 表示使用 AddressSanitizer,能检测地址越界等错误。

然后运行first_fuzzer后有一些打印信息。我们来分段解析这段打印信息。
```
INFO: Seed: 1515548075
INFO: Loaded 1 modules   (7 inline 8-bit counters): 7 [0x564e50, 0x564e57), 
INFO: Loaded 1 PC tables (7 PCs): 7 [0x5408e0,0x540950), 
INFO: -max_len is not provided; libFuzzer will not generate inputs larger than 4096 bytes
```
- Seed: 1515548075 说明这次的种子数据
- max_len:用于设置最大的数据长度，默认为 4096 bytes  ，这个也就是LLVMFuzzerTestOneInput的Size的最大值
- corpus： 不提供预料语，程序会从默认的空语料库开始测试，本例子过于简单，就无需语料库了。

```
#2	   INITED cov: 3 ft: 4 corp: 1/1b lim: 4 exec/s: 0 rss: 37Mb
#22	  NEW    cov: 4 ft: 5 corp: 2/4b lim: 4 exec/s: 0 rss: 37Mb L: 3/3 MS: 5 ChangeBit-ChangeBit-ShuffleBytes-InsertByte-InsertByte-
#693	 NEW    cov: 5 ft: 6 corp: 3/7b lim: 4 exec/s: 0 rss: 37Mb L: 3/3 MS: 1 ChangeBit-
#2740	NEW    cov: 6 ft: 7 corp: 4/10b lim: 6 exec/s: 0 rss: 38Mb L: 3/3 MS: 2 ChangeByte-ChangeBit-
#5127	NEW    cov: 7 ft: 8 corp: 5/18b lim: 8 exec/s: 0 rss: 38Mb L: 8/8 MS: 2 InsertByte-InsertRepeatedBytes-
#5185	REDUCE cov: 7 ft: 8 corp: 5/16b lim: 8 exec/s: 0 rss: 38Mb L: 6/6 MS: 3 CopyPart-CopyPart-EraseBytes-
#5223	REDUCE cov: 7 ft: 8 corp: 5/14b lim: 8 exec/s: 0 rss: 38Mb L: 4/4 MS: 3 ChangeByte-CrossOver-EraseBytes-
```
上面的显示了libFuzzer的运行状态信息
- READ：模糊器已从语料库目录中读取了所有提供的输入样本。
- INITED：模糊器已完成初始化，包括通过被测代码运行每个初始输入样本。
- NEW：模糊测试器创建了一个测试输入，该输入涵盖了被测代码的新区域。此输入将保存到主要语料库目录。
- REDUCE：模糊器发现了更好的（较小的）输入，可以触发先前发现的功能（设置-reduce_inputs=0为禁用）。
- pulse：模糊器已生成2 n个输入（定期生成以使用户确信模糊器仍在工作）。
- DONE：模糊器已完成操作，因为它已达到指定的迭代限制（-runs）或时间限制（-max_total_time）。
- RELOAD：模糊器正在定期从语料库目录中重新加载输入；这使它能够发现其他模糊器进程发现的任何输入（请参阅并行模糊化）。

```
#0 0x5244a2 in DoSomethingInterestingWithMyAPI(unsigned char const*, unsigned long) /home/zison/work/fuzzy/libfuzzer-workshop-master/test/first.cc:19:14
#1 0x524544 in LLVMFuzzerTestOneInput /home/zison/work/fuzzy/libfuzzer-workshop-master/test/first.cc:27:3
#2 0x42ed9a in fuzzer::Fuzzer::ExecuteCallback(unsigned char const*, unsigned long) (/home/zison/work/fuzzy/libfuzzer-workshop-master/test/first_fuzzer+0x42ed9a)
#3 0x42e335 in fuzzer::Fuzzer::RunOne(unsigned char const*, unsigned long, bool, fuzzer::InputInfo*, bool*) (/home/zison/work/fuzzy/libfuzzer-workshop-master/test/first_fuzzer+0x42e335)
#4 0x43007e in fuzzer::Fuzzer::MutateAndTestOne() (/home/zison/work/fuzzy/libfuzzer-workshop-master/test/first_fuzzer+0x43007e)
#5 0x430d55 in fuzzer::Fuzzer::Loop(std::vector<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, fuzzer::fuzzer_allocator<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > const&) (/home/zison/work/fuzzy/libfuzzer-workshop-master/test/first_fuzzer+0x430d55)
#6 0x426d80 in fuzzer::FuzzerDriver(int*, char***, int (*)(unsigned char const*, unsigned long)) (/home/zison/work/fuzzy/libfuzzer-workshop-master/test/first_fuzzer+0x426d80)
#7 0x44a392 in main (/home/zison/work/fuzzy/libfuzzer-workshop-master/test/first_fuzzer+0x44a392)
#8 0x7f3e2d7ae09a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)
#9 0x420899 in _start (/home/zison/work/fuzzy/libfuzzer-workshop-master/test/first_fuzzer+0x420899)
```
上面显示了libFuzzer的执行调用栈，从这段中可以看出 LLVMFuzzerTestOneInput是被libFuzzer的框架代码循环调用的。
```
==22426==ABORTING
MS: 1 EraseBytes-; base unit: 0f27f2bf3d00e604fd40184106d989147f5673e9
0x46,0x55,0x5a,
FUZ
artifact_prefix='./'; Test unit written to ./crash-0eb8e4ed029b774d80f2b66408203801cb982a60
Base64: RlVa
```
以上我们能看到导致程序崩溃的数据，以及testcase输出的路径。

## 复杂示例
本节我们通过一个实实在在的项目，更加深入的了解libFuzzer。
2014年openssl库爆出"心脏流血"漏洞，数以万计的服务器被攻击。今天我们就尝试利用libFuzzer在openssl1.0.1f版本中尝试重现此漏洞

### 构建openssl库
```bash
tar xzf openssl1.0.1f.tgz
cd openssl1.0.1f/

./config
make clean
make CC="clang -O2 -fno-omit-frame-pointer -g -fsanitize=address -fsanitize-coverage=trace-pc-guard,trace-cmp,trace-gep,trace-div" -j$(nproc)
```
### 编译运行Fuzzer

我们需要自己编写一个测试程序，在libFuzzer的入口函数LLVMFuzzerTestOneInput 中调用 openssl的接口,
代码如下：
```c
// Copyright 2016 Google Inc. All Rights Reserved.
// Licensed under the Apache License, Version 2.0 (the "License");
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <assert.h>
#include <stdint.h>
#include <stddef.h>

#ifndef CERT_PATH
# define CERT_PATH
#endif

SSL_CTX *Init() {
  SSL_library_init();
  SSL_load_error_strings();
  ERR_load_BIO_strings();
  OpenSSL_add_all_algorithms();
  SSL_CTX *sctx;
  assert (sctx = SSL_CTX_new(TLSv1_method()));
  /* These two file were created with this command:
      openssl req -x509 -newkey rsa:512 -keyout server.key \
     -out server.pem -days 9999 -nodes -subj /CN=a/
  */
  assert(SSL_CTX_use_certificate_file(sctx, CERT_PATH "server.pem",
                                      SSL_FILETYPE_PEM));
  assert(SSL_CTX_use_PrivateKey_file(sctx, CERT_PATH "server.key",
                                     SSL_FILETYPE_PEM));
  return sctx;
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {
  static SSL_CTX *sctx = Init();
  SSL *server = SSL_new(sctx);
  BIO *sinbio = BIO_new(BIO_s_mem());
  BIO *soutbio = BIO_new(BIO_s_mem());
  SSL_set_bio(server, sinbio, soutbio);
  SSL_set_accept_state(server);
  BIO_write(sinbio, Data, Size);
  SSL_do_handshake(server);
  SSL_free(server);
  return 0;
}
```

编译fuzzer:

```bash
cd ..
clang++ -g openssl_fuzzer.cc -O2 -fno-omit-frame-pointer -fsanitize=address,fuzzer \
    -Iopenssl1.0.1f/include openssl1.0.1f/libssl.a openssl1.0.1f/libcrypto.a \
    -o openssl_fuzzer
```

运行fuzzer:

```bash
mkdir corpus1
./openssl_fuzzer ./corpus1/
```

运行一段时间后:

```
INFO: Seed: 3620533608
INFO: Loaded 1 modules (88608 guards): [0xcad4c0, 0xd03d40), 
Loading corpus dir: ./corpus1/
INFO: -max_len is not provided, using 64
INFO: A corpus is not provided, starting from an empty corpus
#0  READ units: 1
#1  INITED cov: 1473 ft: 385 corp: 1/1b exec/s: 0 rss: 31Mb
#2  NEW    cov: 1479 ft: 414 corp: 2/36b exec/s: 0 rss: 31Mb L: 35 MS: 1 InsertRepeatedBytes-
#111  NEW    cov: 1479 ft: 417 corp: 3/79b exec/s: 0 rss: 36Mb L: 43 MS: 5 ShuffleBytes-CrossOver-CMP-EraseBytes-EraseBytes- DE: "\x00\x00"-
#2256 NEW    cov: 1490 ft: 439 corp: 4/128b exec/s: 0 rss: 127Mb L: 49 MS: 5 PersAutoDict-ChangeBit-ChangeBinInt-ChangeBinInt-InsertRepeatedBytes- DE: "\x00\x00"-

<...>

#120643 NEW    cov: 1563 ft: 721 corp: 37/1736b exec/s: 24128 rss: 373Mb L: 46 MS: 2 CopyPart-CMP- DE: "\x00\x00\x00\x00\x00\x00\x00v"-
#121008 NEW    cov: 1565 ft: 723 corp: 38/1758b exec/s: 24201 rss: 373Mb L: 22 MS: 2 ChangeBinInt-EraseBytes-
=================================================================
==32104==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x629000009748 at pc 0x0000004aad87 bp 0x7fff9266d020 sp 0x7fff9266c7d0
READ of size 25344 at 0x629000009748 thread T0
    #0 0x4aad86 in __asan_memcpy (/usr/local/google/home/mmoroz/Projects/libfuzzer-workshop/lessons/HB/openssl_fuzzer+0x4aad86)
    #1 0x4ff232 in tls1_process_heartbeat /usr/local/google/home/mmoroz/Projects/libfuzzer-workshop/lessons/HB/openssl1.0.1f/ssl/t1_lib.c:2586:3
    #2 0x580be0 in ssl3_read_bytes /usr/local/google/home/mmoroz/Projects/libfuzzer-workshop/lessons/HB/openssl1.0.1f/ssl/s3_pkt.c:1092:4
    #3 0x585c37 in ssl3_get_message /usr/local/google/home/mmoroz/Projects/libfuzzer-workshop/lessons/HB/openssl1.0.1f/ssl/s3_both.c:457:7
    #4 0x548a54 in ssl3_get_client_hello /usr/local/google/home/mmoroz/Projects/libfuzzer-workshop/lessons/HB/openssl1.0.1f/ssl/s3_srvr.c:941:4
    #5 0x544a4e in ssl3_accept /usr/local/google/home/mmoroz/Projects/libfuzzer-workshop/lessons/HB/openssl1.0.1f/ssl/s3_srvr.c:357:9
    #6 0x4f0d42 in LLVMFuzzerTestOneInput /usr/local/google/home/mmoroz/Projects/libfuzzer-workshop/lessons/HB/openssl_fuzzer.cc:39:3
    <...>
```
经过一段时间的测试(测试时间取决于你的电脑性能，可能会持续几天)，在输出的最后我们可以看到最后通过ASan发现了一处溢出。
所处的位置正是tls1_process_heartbeat。

## libFuzzer高级特性

### 1. 字典
LibFuzzer支持用户提供的带有输入语言关键字或其他特殊字节序列（例如，多字节魔术值）的词典。使用-dict=DICTIONARY_FILE。对于某些输入语言，使用字典可能会大大提高搜索速度。字典语法类似于AFL所使用的-x选项：
```
# 以'#'开头 或者 空行被忽略

# 添加blah 
kw1="blah"
# Use \\ for backslash and \" for quotes.
kw2="\"ac\\dc\""
# Use \xAB for hex values
kw3="\xF7\xF8"
# the name of the keyword followed by '=' may be omitted:
"foo\x0Abar"
```
### 生成代码覆盖率报告
使用`-dump_coverage`参数。
```
03:28 zison@zison-PC:08 $ ./xml_read_memory_fuzzer corpus1_min -runs=0 -dump_coverage=1
INFO: Seed: 2354910000
..............
..............
./xml_read_memory_fuzzer.69494.sancov: 1603 PCs written
```
然后会生成一个 *.sancov 文件
```
03:26 zison@zison-PC:08 $ ls *.sancov
xml_read_memory_fuzzer.69494.sancov
```
然后把它转换成 .symcov
```
sancov -symbolize xml_read_memory_fuzzer ./xml_read_memory_fuzzer.69494.sancov > xml_read_memory_fuzzer.symcov
```
然后使用 coverage-report-server 解析这个文件。
```
03:29 zison@zison-PC:08 $ python3 coverage-report-server.py --symcov xml_read_memory_fuzzer.symcov     --srcpath libxml2
Loading coverage...
Serving at 127.0.0.1:8001
```
用浏览器访问此地址

## 总结
本文中，我们了解了libFuzzer的基本概念以及简单用法。关于模糊目标需要记住的一些重要事项：
- 模糊引擎将在同一过程中使用不同的输入多次执行模糊目标。
- 它必须容忍任何形式的输入（空的，巨大的，格式错误的等）。
- 它不能在任何输入上退出（）。
- 它可以使用线程，但理想情况下，所有线程都应在函数末尾连接。
- 它必须尽可能具有确定性。非确定性（例如random，不基于输入字节的随机决策）将使模糊效率低下。
- 一定要快。尝试避免三次或更长时间的复杂性，日志记录或过多的内存消耗。
- 理想情况下，它不应修改任何全局状态（尽管并不严格）。
- 通常，目标越窄越好。例如，如果您的目标可以解析多种数据格式，请将其拆分为多种目标，每种格式一个。

除了libFuzzer ,常用的还有AFL,Peach Fuzz 等。其中 AFL和libFuzzer 使用较多。我们就讲解下libFuzzer 与AFL 区别。

AFL可与gcc或clang一起使用，但libFuzzer由LLVM开发，仅clang支持。
libfuzzer 需要手动编写一个int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size)`函数作为接口作为Fuzz的入口。 而 afl   只需要 stdin 与 stdout 即可。由于libFuzzer是在进程内多次调用一个函数入口，比AFL省去了进程创建，销毁的开销，所以效率会比AFL快些。同时libFuzeer适用于模块初始化比较耗时的测试，类似下面：
```
bool inited = false;
int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size)
{
	if (!inited)
   {
   	init(); 
       inited = true;
   }
   test(data,size);
}
```
libFuzzer的缺点就是因为在同一进程内，会存在全局污染的问题。例如某一程序使用了一个全局计数，达到一定的值就退出，这种情况下，就会导致问题。