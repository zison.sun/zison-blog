# libFuzzer及AFL使用指南
> 随便测试方法以及测试手段的提高，软件中BUG 越来越少，但是不代表软件系统是绝对安全的。其中隐藏的一些触发概率极小BUG，单凭人工很难发现。针对这种情况，Fuzz测试应运而生。Fuzz测试是一种通过向软件(模块)输入大量不同的数据，观测被测试软件是否异常的测试方法。 本文将带领大家了解模糊测试基本概念以及常用工具的使用方法。

## libFuzzer

### libFuzzer简介
libFuzzer 是一个*in-process，coverage-guided，evolutionary*的 fuzz 引擎，是 LLVM 项目的一部分。libFuzzer 和 要被测试的库 链接在一起，通过一个模糊测试入口点（目标函数），把测试用例喂给要被测试的库。fuzzer会跟踪哪些代码区域已经测试过，然后在输入数据的语料库上进行变异，来使代码覆盖率最大化。代码覆盖率的信息由 LLVM 的 SanitizerCoverage 插桩提供。

Clang的最新版本（从6.0开始）包括libFuzzer，为了构建Fuzzer，需要在编译和链接期间使用-fsanitize=fuzzer标志。
```
clang -g -O1 -fsanitize=fuzzer                         mytarget.c # Builds the fuzz target w/o sanitizers
clang -g -O1 -fsanitize=fuzzer,address                 mytarget.c # Builds the fuzz target with ASAN
clang -g -O1 -fsanitize=fuzzer,signed-integer-overflow mytarget.c # Builds the fuzz target with a part of UBSAN
clang -g -O1 -fsanitize=fuzzer,memory                  mytarget.c # Builds the fuzz target with MSAN
```
注意，我们发现 每个代码没有main 函数入口，是因为-fsanitize=fuzzer 将会链接 libFuzzer中的 main 函数。<br>

以下通过一个实例了解libFuzzer的使用:<br>
**first_fuzzer.cc:**
```
#include <stdint.h>
#include <stddef.h>

bool VulnerableFunction1(const uint8_t* data, size_t size) {
  bool result = false;
  if (size >= 3) {
    result = data[0] == 'F' &&
             data[1] == 'U' &&
             data[2] == 'Z' &&
             data[3] == 'Z';
  }
  return result;
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
  VulnerableFunction1(data, size);
  return 0;
}
```
编译Fuzzer:
`clang++ -g -std=c++11 -fsanitize=address,fuzzer first_fuzzer.cc -o first_fuzzer` 

运行生成的`./first_fuzzer`
```
INFO: Seed: 3010836021
INFO: Loaded 1 modules   (35 inline 8-bit counters): 35 [0x567ed0, 0x567ef3), 
INFO: Loaded 1 PC tables (35 PCs): 35 [0x5429a8,0x542bd8), 
INFO: -max_len is not provided; libFuzzer will not generate inputs larger than 4096 bytes
INFO: A corpus is not provided, starting from an empty corpus
#2	INITED cov: 3 ft: 4 corp: 1/1b lim: 4 exec/s: 0 rss: 42Mb
#17	NEW    cov: 4 ft: 5 corp: 2/4b lim: 4 exec/s: 0 rss: 42Mb L: 3/3 MS: 5 ChangeBit-ShuffleBytes-ChangeBit-CopyPart-InsertByte-
#511	NEW    cov: 5 ft: 6 corp: 3/8b lim: 4 exec/s: 0 rss: 42Mb L: 4/4 MS: 4 CrossOver-ShuffleBytes-InsertByte-CMP- DE: "F\x00"-
#542	REDUCE cov: 5 ft: 6 corp: 3/7b lim: 4 exec/s: 0 rss: 42Mb L: 3/3 MS: 1 EraseBytes-
#4418	REDUCE cov: 6 ft: 7 corp: 4/10b lim: 6 exec/s: 0 rss: 42Mb L: 3/3 MS: 1 ChangeByte-
=================================================================
省略若干打印
...
==27274==ABORTING
MS: 3 ChangeBinInt-CopyPart-ChangeBit-; base unit: 2beb23b3e301a9041a6e65ec2e5250a2ee4678c7
0x46,0x55,0x5a,
FUZ
artifact_prefix='./'; Test unit written to ./crash-0eb8e4ed029b774d80f2b66408203801cb982a60
Base64: RlVa
```
在本例中，libffuzer 通过不断的调用LLVMFuzzerTestOneInput，传入不同的data，以及size，来尝试触发VulnerableFunction1中不同的代码路径，我们可以看到最终`FUZ`的输入导致了程序终止，导致错误的输入也写入了`./crash-0eb8e4ed029b774d80f2b66408203801cb982a60`中，我们也可以使用`xxd ./crash-0eb8e4ed029b774d80f2b66408203801cb982a60`来查看。
我们注意到程序提示-max_len 未提供，默认使用4096。-max_len其实是这是size最大值的。<br>
同时，我们还看到提示了**语料库**(corpus)未提供,语料库是模糊测试的输出输出，模糊测试在语料库的基础上进行变异，好的语料库能大大提升模糊测试的速度。由于本实例比较简单，这里就不使用语料库了。

### 语料库
语料库(Corpus)是输入数据的样本，或者种子，模糊测试引擎可以给予语料库进行变异。如果被测试程序接受复杂的结构化输入，例如输入的是个图片，PDF，他们有固定的文件头结构，那么提供一些精简的图片，pdf作为语料库，模糊器基于当前语料库中的样本输入生成随机突变。如果突变触发了测试代码中先前未覆盖的路径的执行，则该突变将保存到语料库中以供将来变更。良好的语料库将大大提高fuzzy的速度。

#### 语料库的使用













