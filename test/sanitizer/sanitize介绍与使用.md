# sanitizer工具集的介绍与使用

## 概述
### 简介：	
Sanitizers是谷歌发起的开源工具集，包括了Address Sanitizer, undefined behavior Sanitizer, Thread Sanitizer, Leak Sanitizer。GCC从4.8版本开始支持Address sanitizer和Thread Sanitizer，4.9版本开始支持Leak Sanitizer和undefined behavior Sanitizer。

- Address Sanitizer（ASAN）:
也即地址消毒技术，通过编译插桩(CTI)，能够发现此堆/栈/全局变量读写溢出，内存泄露等问题，并将信息直接打印到日志中。
Address Sanitizer（ASan）是一个快速的内存错误检测工具。它非常快，只拖慢程序两倍左右（比起Valgrind快多了）。
它包括一个编译器instrumentation模块和一个提供malloc()/free()替代项的运行时库。Thread Sanitizer（TSan）:
是一个检查线程Data Race的C/C++工具。
- Leak Sanitizer（LSan）:
检测内存的LeakSanitizer是集成在Address Sanitizer中的一个相对独立的工具，它工作在检查过程的最后阶段。
- Undefiend Behavior Sanitizer（UBSan）:
检测未定义行为（使用空指针、有符号整数溢出等）。

## 安装与环境配置
### ASan、UBSan、LSan环境配置：

** QMake环境配置：**
在pro文件中添加：
```
QMAKE_CXXFLAGS+="-fsanitize=undefined,address,leak -fno-omit-frame-pointer"
QMAKE_CFLAGS+="-fsanitize=undefined,address,leak -fno-omit-frame-pointer"
QMAKE_LFLAGS+="-fsanitize=undefined,address,leak -fno-omit-frame-pointer"
```
用-fsanitize=address选项，编译和链接你的程序。<br>
用undefined,可以使用undefined behavior sanitizer检测未定义行为。<br>
用leak,可以使用leak sanitizer检测内存泄露（很多平台默认关闭,X86默认开启的）。<br>
用-fno-omit-frame-pointer(与相对)编译，以得到更容易理解stack trace。<br>
注：-fomit-frame-pointer是打开优化选项(-O1打开)，与-fno-omit-frame-pointer相反，即在函数调用时不保存栈帧指针SFP，代价是不能通过backtrace进行调试根据堆栈信息了。<br>

**CMake环境配置：**
```
在CMakeLists添加：
set(CMAKE_CXX_FLAGS "-fsanitize=undefined,address,leak -fno-omit-frame-pointer")
set(CMAKE_C_FLAGS "-fsanitize=undefined,address,leak -fno-omit-frame-pointer")
set(CMAKE_Ｌ_FLAGS "-fsanitize=undefined,address,leak -fno-omit-frame-pointer")
```

### TSan环境配置：
主要用于检测多线程资源竞争的问题，**但是该选项不能与-fsanitize=address、-fsanitize=leak组合**。

** QMake环境配置：**
```
在pro文件中添加：
QMAKE_CXXFLAGS+="-fsanitize=thread"
QMAKE_CFLAGS+="-fsanitize=thread"
QMAKE_LFLAGS+="-fsanitize=thread"
```
** CMake环境配置：**
```
在CMakeLists添加：
set(CMAKE_CXX_FLAGS "-fsanitize=thread")
set(CMAKE_C_FLAGS "-fsanitize=thread")
set(CMAKE_Ｌ_FLAGS "-fsanitize=thread")
```

可以看出无论是什么工程，无非就是给cflags,ldflags 增加-fsanitize 等选项。

## 工具原理及使用方法
### 原理：
Address Sanitizer替换了malloc和free的实现。当调用malloc函数时，它将分配指定大小的内存A，并将内存A周围的区域标记为”off-limits“。当free方法被调用时，内存A也被标记为”off-limits“，同时内存A被添加到隔离队列，这个操作将导致内存A无法再被重新malloc使用。
当访问到被标记为”off-limits“的内存时，Address Sanitizer就会报告异常。
![原理图](./sanitize1.png)

### 错误类型：
在工作中我们经常会碰到程序异常崩溃，大部分都是以下内存错误，引起的可以使用此工具进行检测，帮助快速定位。<br>
- Use after free 	释放后使用
- Heap buffer overflow　	堆缓冲区溢出
- Stack buffer overflow　栈缓冲区溢出
- Global buffer overflow　全局缓冲区溢出
- Use after return　返回后使用
- Use after scope　作用域后使用
- Initialization order bugs　初始化顺序错误
- Memory leaks　内存泄露
- Using misaligned or null pointer 　使用未对齐的指针
- Signed integer overflow　　有符号整数溢出
- Conversion to, from, or between floating-point types which would overflow the destination　　和浮点数相关转换溢出
- data race 　数据竞争

### Address Sanitizer：
#### 错误类型：use after free
测试代码：
```
# Address_Sanitizer1.cc
int main(){
    int* array = new int[100];
    delete [] array;
    return array[1];
}
```
编译：`clang++ -g Address_Sanitizer1.cc -fsanitize=address -o Address_Sanitizer1`

运行打印：
```
zison@zison-PC:~/work/fuzzy/libfuzzer-workshop-master/test$ ./Address_Sanitizer1 
=================================================================
==26588==ERROR: AddressSanitizer: heap-use-after-free on address 0x614000000044 at pc 0x0000004f82f4 bp 0x7ffe3ceeff80 sp 0x7ffe3ceeff78
READ of size 4 at 0x614000000044 thread T0
    #0 0x4f82f3 in main /home/zison/work/fuzzy/libfuzzer-workshop-master/test/Address_Sanitizer1.cc:4:12
    #1 0x7f9a9bad109a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)
    #2 0x41e2d9 in _start (/home/zison/work/fuzzy/libfuzzer-workshop-master/test/Address_Sanitizer1+0x41e2d9)

0x614000000044 is located 4 bytes inside of 400-byte region [0x614000000040,0x6140000001d0)
freed by thread T0 here:
    #0 0x4f6122 in operator delete[](void*) (/home/zison/work/fuzzy/libfuzzer-workshop-master/test/Address_Sanitizer1+0x4f6122)
    #1 0x4f82ac in main /home/zison/work/fuzzy/libfuzzer-workshop-master/test/Address_Sanitizer1.cc:3:5
    #2 0x7f9a9bad109a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)

previously allocated by thread T0 here:
    #0 0x4f54f2 in operator new[](unsigned long) (/home/zison/work/fuzzy/libfuzzer-workshop-master/test/Address_Sanitizer1+0x4f54f2)
    #1 0x4f828a in main /home/zison/work/fuzzy/libfuzzer-workshop-master/test/Address_Sanitizer1.cc:2:18
    #2 0x7f9a9bad109a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)

SUMMARY: AddressSanitizer: heap-use-after-free /home/zison/work/fuzzy/libfuzzer-workshop-master/test/Address_Sanitizer1.cc:4:12 in main
Shadow bytes around the buggy address:
...省略...
==26588==ABORTING
```
其中
```
SUMMARY: AddressSanitizer: heap-use-after-free /home/zison/work/fuzzy/libfuzzer-workshop-master/test/Address_Sanitizer1.cc:4:12 in main
```
显示了错误类型：heap-use-after-free 。以及关键出错位置：Address_Sanitizer1.cc:4。
前面的输出现实了堆上内存的分配以及释放。

#### 错误类型：heap buffer overflow
测试代码：
```
int main(){
    int* array = new int[100];
    int res = array[100];
    delete [] array;
    return res;
}
```
编译运行打印显示:
```
SUMMARY: AddressSanitizer: heap-buffer-overflow /home/zison/work/fuzzy/libfuzzer-workshop-master/test/Address_Sanitizer2.cc:3:15 in main
```
可以看到错误类型：heap-buffer-overflow 。以及关键出错位置：Address_Sanitizer2.cc:3。

#### 错误类型：stack-buffer-overflow
测试代码：
```
int main(){
	int array[100];
	return array[100];
}
```
编译运行打印显示:
```
AddressSanitizer: stack-buffer-overflow /home/zison/work/fuzzy/libfuzzer-workshop-master/test/Address_Sanitizer3.cc:3:9 in main
```
可以看到错误类型：stack-buffer-overflow 。以及关键出错位置：Address_Sanitizer3.cc:3。

#### 错误类型：global buffer overflow

测试代码：
```
int array[100];
int main(){
	return array[100];
}
```
编译运行打印显示:
```
SUMMARY: AddressSanitizer: global-buffer-overflow /home/zison/work/fuzzy/libfuzzer-workshop-master/test/Address_Sanitizer4.cc:3:9 in main
```
可以看到错误类型：stack-buffer-overflow 。以及关键出错位置：Address_Sanitizer4.cc:3。

### Undefined Behavior Sanitizer:
#### 错误类型：using null pointer

测试代码：
```
void usernullpointer()
{
    char* p = nullptr;
    char kd = *p;
}

int main()
{
    usernullpointer();
    return 0;
}
```
编译：`clang++ -g UndefinedSanitizer1.cc -fsanitize=undefined -o UndefinedSanitizer1`
运行打印显示:
```
zison@zison-PC:~/work/fuzzy/libfuzzer-workshop-master/test$ ./UndefinedSanitizer1 
UndefinedSanitizer1.cc:4:15: runtime error: load of null pointer of type 'char'
UndefinedBehaviorSanitizer:DEADLYSIGNAL
==27933==ERROR: UndefinedBehaviorSanitizer: SEGV on unknown address 0x000000000000 (pc 0x000000423e8d bp 0x7ffc5fd512f0 sp 0x7ffc5fd512d0 T27933)
==27933==The signal is caused by a READ memory access.
==27933==Hint: address points to the zero page.
    #0 0x423e8c in main /home/zison/work/fuzzy/libfuzzer-workshop-master/test/UndefinedSanitizer1.cc:4:15
    #1 0x7f6d6684909a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)
    #2 0x403319 in _start (/home/zison/work/fuzzy/libfuzzer-workshop-master/test/UndefinedSanitizer1+0x403319)

UndefinedBehaviorSanitizer can not provide additional info.
==27933==ABORTING
```
可以看到 UndefinedSanitizer1.cc:4:15 检测到了 UndefinedBehaviorSanitizer

#### 错误类型：signed integer overflow
测试代码：
```
void testsignoverflow()
{
    int k = 0x7fffffff;
    k += 2;
}
```

```
UndefinedSanitizer2.cc:4：15: runtime error: signed integer overflow: 2147483647 + 2 cannot be represented in type 'int'
```
可以看到 检测到了 signed integer overflow。

### Leak Sanitizer:
#### 错误类型：detected memory leaks
测试代码：
```
void* p;
int main(){
    p = malloc(4);
    p = 0;
    return 0;
}
```
编译：`clang++ -g UndefinedSanitizer1.cc -fsanitize=leak -o UndefinedSanitizer1`

运行现实
```
==29344==ERROR: LeakSanitizer: detected memory leaks
```
其实内存泄漏，使用gcc的 --wrap 自己实现包装函数，通过__wrap_malloc传入 源码位置，保存在多分配的内存空间中。也能达到检测内存泄漏的目的。有机会我们在单独讲解。

### Thread Sanitizer:
#### 错误类型：data race
测试代码：
```
#include <stdio.h>
#include <pthread.h>

int Global;
void *Thread1(void *x) {
  Global++;
  return NULL;
}
void *Thread2(void *x) {
  Global--;
  return NULL;
}
int main(){
    pthread_t t[2];
    pthread_create(&t[0], NULL, Thread1, NULL);
    pthread_create(&t[1], NULL, Thread2, NULL);
    pthread_join(t[0], NULL);
    pthread_join(t[1], NULL);
}

```
编译``
运行
```
==================
WARNING: ThreadSanitizer: data race (pid=30254)
  Write of size 4 at 0x000000f8f5c8 by thread T2:
    #0 Thread2(void*) /home/zison/work/fuzzy/libfuzzer-workshop-master/test/thread_sanitize.cc:10:9 (Thread_Sanitize+0x4b0b2e)

  Previous write of size 4 at 0x000000f8f5c8 by thread T1:
    #0 Thread1(void*) /home/zison/work/fuzzy/libfuzzer-workshop-master/test/thread_sanitize.cc:6:9 (Thread_Sanitize+0x4b0ace)

  Location is global 'Global' of size 4 at 0x000000f8f5c8 (Thread_Sanitize+0x000000f8f5c8)

  Thread T2 (tid=30257, running) created by main thread at:
    #0 pthread_create <null> (Thread_Sanitize+0x426795)
    #1 main /home/zison/work/fuzzy/libfuzzer-workshop-master/test/thread_sanitize.cc:16:5 (Thread_Sanitize+0x4b0ba0)

  Thread T1 (tid=30256, finished) created by main thread at:
    #0 pthread_create <null> (Thread_Sanitize+0x426795)
    #1 main /home/zison/work/fuzzy/libfuzzer-workshop-master/test/thread_sanitize.cc:15:5 (Thread_Sanitize+0x4b0b83)

SUMMARY: ThreadSanitizer: data race /home/zison/work/fuzzy/libfuzzer-workshop-master/test/thread_sanitize.cc:10:9 in Thread2(void*)
==================
ThreadSanitizer: reported 1 warnings
```
可以看到提示存在数据竞争，这时候就考虑是否需要通过添加锁等方法来实现线程同步了。

### 总结：
ASAN、LSan、UBSan:
对可能出现内存泄露、访问越界、堆栈溢出，可以使用此三种工具同时检查，建议在每次提交代码之前，开启此三项检查，可以排除大部分常见错误，项目不大的话也可以配置到debug里。
	
Thread Sanitizer（TSan）:
由于此工具会和其他工具组合冲突，建议在新增线程或者线程中可能出现data trace的情况下使用。例如：出现多线程的线程安全问题，可以开启此工具检查。

错误输出：
在正常的项目开发中，会有存有大量的日志信息输出到应用程序输出里，这样会加大查找错误信息的难度，因此建议在将sanitizer错误信息输出到日志里。
#include <sanitizer/asan_interface.h>
__sanitizer_set_report_path("asan.log");
在指定的目录会生成一个asan.log.pid(进程号)的文件。

总结：sanitizer工具集使用较为方便，编译效率较高，检测比较全面。开发人员可以在软件开发过程中合理使用此项技术以提高代码质量。