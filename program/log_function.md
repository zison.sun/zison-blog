```

enum
{
    LOG_NONE,
    LOG_FATAL,
    LOG_ERROR,
    LOG_WARN,
    LOG_INFO,
    LOG_DEBUG,
    LOG_TRACE,
    LOG_DUMP,
};

#define OUTPUT_STDOUT

#ifdef OUTPUT_STDOUT
#include <stdio.h>
extern FILE *stdout;
#define ftrace stdout
#else
extern FILE* ftrace;
#endif

#define log_lock()
#define log_unlock()

#define log_log(level,color,levelstr,file,line,tid,fmt, ...)    do {                                            \
                                                                    if(level <= dynarec_level) {                \
                                                                        char time_str[50];                      \
                                                                        char * filename = strrchr(file,'/');    \
                                                                                                                \
                                                                        log_lock();                             \
                                                                        time_t tm = time(NULL);                 \
                                                                        strftime(time_str, 50, "%Y%m%d%H%M%S", localtime(&tm));                     \
                                                                        fprintf(ftrace,color"[%5s][%04d][%s][%s:%d]",levelstr,tid,time_str,filename?filename+1:file,line);   \
                                                                        fprintf(ftrace, fmt,##__VA_ARGS__);                             \
                                                                        fprintf(ftrace, LOCAL_END"\n");                                 \
                                                                        fflush(ftrace);                                                 \
                                                                        log_unlock();                                                   \
                                                                    }                                                                   \
                                                                } while(0)


#define log_print(fmt, ...)  do {                                    \
                                if(LOG_DUMP <= dynarec_level) {     \
                                    log_lock();                     \
                                    fprintf(ftrace, fmt,##__VA_ARGS__);                             \
                                    fflush(ftrace);                                                 \
                                    log_unlock();                                                   \
                                }                                                                   \
                            } while(0)



#define LOCAL_RED1      "\033[41;37m"
#define LOCAL_RED2      "\033[0;31m"
#define LOCAL_GREEN     "\033[0;32m"
#define LOCAL_YELLOW    "\033[0;33m"
#define LOCAL_BULE      "\033[0;34m"
#define LOCAL_PURPLE    "\033[0;35m"
#define LOCAL_CYAN      "\033[0;36m"
#define LOCAL_WHITE     "\033[0;37m"


#define LOCAL_END       "\033[0m"


#define log_dump(fmt,...)  log_log(LOG_DUMP, LOCAL_WHITE, "dump",  __FILE__, __LINE__,GetTID(),fmt , ##__VA_ARGS__)
#define log_trace(fmt,...) log_log(LOG_TRACE,LOCAL_BULE,  "trace", __FILE__, __LINE__,GetTID(),fmt , ##__VA_ARGS__)
#define log_debug(fmt,...) log_log(LOG_DEBUG,LOCAL_CYAN,  "debug", __FILE__, __LINE__,GetTID(),fmt , ##__VA_ARGS__)
#define log_info(fmt,...)  log_log(LOG_INFO, LOCAL_GREEN, "info",  __FILE__, __LINE__,GetTID(),fmt , ##__VA_ARGS__)
#define log_warn(fmt,...)  log_log(LOG_WARN, LOCAL_YELLOW,"warn",  __FILE__, __LINE__,GetTID(),fmt , ##__VA_ARGS__)
#define log_error(fmt,...) log_log(LOG_ERROR,LOCAL_RED2,  "error", __FILE__, __LINE__,GetTID(),fmt , ##__VA_ARGS__)
#define log_fatal(fmt,...) log_log(LOG_FATAL,LOCAL_RED1,  "fatal", __FILE__, __LINE__,GetTID(),fmt , ##__VA_ARGS__)

```

