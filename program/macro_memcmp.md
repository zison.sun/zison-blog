```
#define MEMCMP(src,dst,n)   ({								        \
				int i = 0,ret=0;					        \
				for(i=0; i < (n) ; i++)					        \
				{							        \
					if ( *(unsigned char *)((void *)(src) + i)  		\
					    !=  *(unsigned char *)((void *)(dst) +i) )		\
					   ret = 1;					        \
				}							        \
				ret;							        \
			    })
```
